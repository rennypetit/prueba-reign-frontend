# Frontend

**Autor:** Renny Petit
Proyecto desarrollado para la empresa Regin: https://www.reign.cl/es/

## Documentación

El proyecto esta desarrollado con react.js con el framework next.js. Asimismo se creó una imagen de docker en la cual se conectó a la misma red para consumir los datos del backend.

Por otra parte, se realizá el despliegue en gitlab, se corre el CI de pipelines, se ejecuta la instalación de las dependencias y se ejecuta es eslint.

**URL**: https://gitlab.com/rennypetit/prueba-reign-frontend

### Ambiente desarrollo

Utiliza sus propias variables de entorno, Dockerfile y docker-compose.

#### Ejecutar

```bash
 docker-compose up
```

### Ambiente de producción

Utiliza sus propias variables de entorno, Dockerfile y docker-compose, se diferencia del ambiente anterior debido a que este ejecuta un build en su docker.

**Nota:** Debe asegurarse que el build cree la carpeta .next si no la crea puede ejecutar yarn build para crearla y posteriormente ejecutar el comando a continuación.

#### Ejecutar

```bash
  docker-compose -f docker-compose.yml -f production.yml up.
```

### Docker

El proyecto cuenta con 2 archivos de docker y 2 de docker-compose. Asimismo cuenta con el nombre de la red, creada en la imagen del backend.

##### Desarrollo:

- Dockerfile
- docker-compose.yml

##### Producción:

- Dockerfile.production
- production.yml

### Gitlab CI

El CI de gitlab se realiza automáticamente realizando un push al repositorio puede editar la configurar en el archivo .gitlab-ci.yml

### Variables de entorno

El nombre de las variables de entorno las puede encontrar el archivo: .env.example

### Consideraciones

- La red que se encuentra en el contenedor debe ser la misma red del backend.
- El nombre del contenedor del backend se utiliza para la conexión de la API (ver variables de entorno), si el nombre del contenedor backend cambia dicha variable se debe actualizar.
