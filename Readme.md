# Front-End

**Author:** Renny Petit

Project developed for the company Regin: https://www.reign.cl/es/

## Documentation

The project is developed with react.js with the next.js framework. Likewise, a docker image was created in which it was connected to the same network to consume the backend data.

On the other hand, the deployment is carried out in gitlab, the pipeline CI is run, the installation of the dependencies is executed and eslint is executed.

**URL**: https://gitlab.com/rennypetit/prueba-reign-frontend

### development environment

It uses its own environment variables, Dockerfile and docker-compose.

#### Run

```bash
 docker-compose up
```

### Production environment

It uses its own environment variables, Dockerfile and docker-compose, it differs from the previous environment because it executes a build in your docker.

**Note:** You need to make sure the build creates the .next folder if it doesn't you can run yarn build to create it and then run the command below.

#### Run

```bash
  docker-compose -f docker-compose.yml -f production.yml up.
```

###Docker

The project has 2 docker files and 2 docker-compose files. It also has the name of the network, created in the backend image.

##### Developing:

-Docker file

- docker-compose.yml

##### Production:

- Dockerfile.production
  -production.yml

### Gitlab CI

Gitlab CI is done automatically by pushing to the repository you can edit the configure in the .gitlab-ci.yml file

### Environment Variables

The name of the environment variables can be found in the file: .env.example

### Considerations

- The network found in the container must be the same network as the backend.
- The name of the backend container is used for the API connection (see environment variables), if the name of the backend container changes, this variable must be updated.
