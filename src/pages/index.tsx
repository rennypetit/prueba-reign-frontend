import type { GetServerSideProps, NextPage } from 'next';
import MyHead from '@components/Head';
import Banner from '@components/Banner';
import Context from '@components/Context';

const Home: NextPage = ({ data }: any) => {
	return (
		<>
			<MyHead />
			<Banner />
			{data?.statusCode ? (
				<h2>Error in data</h2>
			) : data === null ? (
				<h2>Internal error:</h2>
			) : (
				<Context data={data} />
			)}
		</>
	);
};

export const getServerSideProps: GetServerSideProps = async () => {
	try {
		const res = await fetch(`${process.env.NEXT_PUBLIC_API_BACKEND}/posts`);
		const data = await res.json();
		return { props: { data } };
	} catch (error) {
		console.error('home page:', error);
		return { props: { data: null } };
	}
};

export default Home;
