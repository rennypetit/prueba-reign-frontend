import { NextComponentType } from 'next';
import styles from './index.module.css';
const Banner: NextComponentType = () => {
	return (
		<div className={styles.banner}>
			<div className='global-container'>
				<h1 className={styles.title}>HN Feed</h1>
				<p className={styles.description}>{'We <3 hacker News!'}</p>
			</div>
		</div>
	);
};

export default Banner;
