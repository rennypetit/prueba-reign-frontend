import Head from 'next/head';
import React from 'react';

export default function MyHead() {
	return (
		<Head>
			<title>Test Renny Petit</title>
			<meta name='viewport' content='initial-scale=1.0, width=device-width' />
		</Head>
	);
}
