import Image from 'next/image';
import { formatDistance, subDays } from 'date-fns';
import styles from './index.module.css';
import { useState } from 'react';
import removePost from '@hooks/removePost';
const Context = ({ data }: any) => {
	const [posts, setPosts] = useState(data); // state of posts

	const handleRemove = async (id: string) => {
		const remove = await removePost(id); // remove post of database
		const result = posts.filter((item: any) => item._id !== id);
		// remove post of state
		if (remove === 200) setPosts(result);
		else alert('notfound'); // si el resultado es diferente de 200
	};
	// si no se encuentran posts
	if (data.length === 0)
		return <h1 className={styles.notFound}>posts empty</h1>;
	return (
		<div className={styles.context}>
			<div className='global-container'>
				<ul className={styles.ul}>
					{posts.map((item: Post, index: number) => (
						<li key={index}>
							<div className={styles.list}>
								<a
									href={
										item.content?.story_url !== null
											? item.content?.story_url
											: item.content?.url !== null
											? item.content?.url
											: '#'
									}
									target='_blank'
									rel='noopener noreferrer'
								>
									{item.content?.story_title !== null
										? item.content?.story_title
										: item.content?.title !== null
										? item.content?.title
										: ''}

									<span>- {item.content?.author} - </span>
								</a>
								<div className={styles.right}>
									<p>
										{formatDistance(
											subDays(new Date(item.content.created_at), 0),
											new Date(),
											{
												addSuffix: true,
											}
										)}
									</p>
									<Image
										src='/image/icons/remove.svg'
										alt='icon remove'
										width='16'
										height='16'
										onClick={(e) => handleRemove(item._id)}
									/>
								</div>
							</div>
						</li>
					))}
				</ul>
			</div>
		</div>
	);
};

export interface Post {
	_id: string;
	content: {
		title: string;
		story_title: string;
		author: string;
		created_at: string;
		story_url: string;
		url: string;
	};
	status: number;
}
export default Context;
