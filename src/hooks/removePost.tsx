import { METHODS } from 'http';
import React from 'react';

export default async function removePost(id: string) {
	try {
		const post = await fetch(
			`${process.env.NEXT_PUBLIC_API_BACKEND}/posts/${id}`,
			{
				method: 'PATCH',
			}
		);
		return post.status;
	} catch (error) {
		console.error('error remove:', error);
		alert('error server');
		return 500;
	}
}
